package arquivoDePedido

type ItensPedido struct {
	TipoRegistro              int32   `json:"TipoRegistro"`
	CodigoItem                int32   `json:"CodigoItem"`
	Quantidade                int32   `json:"Quantidade"`
	Desconto                  float32 `json:"Desconto"`
	PrecoUnitario             float32 `json:"PrecoUnitario"`
	FaotrConversao            int32   `json:"FaotrConversao"`
	ChavePolitica             int32   `json:"ChavePolitica"`
	PrecoUnitarioApresentacao float32 `json:"PrecoUnitarioApresentacao"`
	TipoSaida                 string  `json:"TipoSaida"`
}
