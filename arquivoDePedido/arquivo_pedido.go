package arquivoDePedido

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

type ArquivoDePedido struct {
	CabecalhoPedido CabecalhoPedido `json:"CabecalhoPedido"`
	ItensPedido     []ItensPedido   `json:"ItensPedido"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		vector := strings.Split(string(runes), "|")
		identificador := int32(ConvertStringToInt(vector[0]))

		if identificador == 1 {
			cabecalho := CabecalhoPedido{}
			cabecalho.TipoRegistro = int32(ConvertStringToInt(vector[0]))
			cabecalho.CodigoRepresentante = int32(ConvertStringToInt(vector[1]))
			cabecalho.CodigoCliente = int32(ConvertStringToInt(vector[2]))
			cabecalho.DataEmissao = vector[3]
			cabecalho.HoraEmissao = vector[4]
			cabecalho.CodigoPedidoWeb = vector[5]
			cabecalho.Observacao  = vector[6]
			cabecalho.CodigoPrazo  = int32(ConvertStringToInt(vector[7]))
			cabecalho.ChavePolitica = int32(ConvertStringToInt(vector[8]))

			arquivo.CabecalhoPedido = cabecalho

		} else if identificador == 2 {

			itens := ItensPedido{}
			itens.TipoRegistro = int32(ConvertStringToInt(vector[0]))
			itens.CodigoItem = int32(ConvertStringToInt(vector[1]))
			itens.Quantidade = int32(ConvertStringToInt(vector[2]))

			DescontoInteiro :=  float32(ConvertStringToInt(vector[3][0:2]))
			DescontoDecimal := (float32(ConvertStringToInt(vector[3][2:4])))/100
			itens.Desconto = DescontoInteiro + DescontoDecimal

			PrecoUnitarioInteiro :=  float32(ConvertStringToInt(vector[4][0:5]))
			PrecoUnitarioDecimal := (float32(ConvertStringToInt(vector[4][5:9])))/10000
			itens.PrecoUnitario  = PrecoUnitarioInteiro + PrecoUnitarioDecimal

			itens.FaotrConversao= int32(ConvertStringToInt(vector[5]))
			itens.ChavePolitica = int32(ConvertStringToInt(vector[6]))

			PrecoUnitarioApresentacaoInteiro :=  float32(ConvertStringToInt(vector[7][0:5]))
			PrecoUnitarioApresentacaoDecimal := (float32(ConvertStringToInt(vector[7][5:9])))/10000
			itens.PrecoUnitarioApresentacao  = PrecoUnitarioApresentacaoInteiro + PrecoUnitarioApresentacaoDecimal

			itens.TipoSaida = vector[8]

			arquivo.ItensPedido = append(arquivo.ItensPedido, itens)

		}
	}
	return arquivo, err
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}