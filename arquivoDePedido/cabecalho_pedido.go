package arquivoDePedido

type CabecalhoPedido struct {
	TipoRegistro        int32  `json:"TipoRegistro"`
	CodigoRepresentante int32  `json:"CodigoRepresentante"`
	CodigoCliente       int32  `json:"CodigoCliente"`
	DataEmissao         string `json:"DataEmissao"`
	HoraEmissao         string `json:"HoraEmissao"`
	CodigoPedidoWeb     string  `json:"CodigoPedidoWeb"`
	Observacao          string `json:"Observacao"`
	CodigoPrazo         int32  `json:"CodigoPrazo"`
	ChavePolitica       int32  `json:"ChavePolitica"`
}


