package arquivoDeRetorno

type ItensPedido struct {
	IdStatusPedido     int32   `json:"IdStatusPedido"`
	IdProduto          int32   `json:"IdProduto"`
	QuantidadeComprada int32   `json:"QuantidadeComprada"`
	QuantidadeFaturada int32   `json:"QuantidadeFaturada"`
	ValorFaturado      float32 `json:"ValorFaturado"`
}
