package arquivoDeRetorno

type StatusPedido struct {
	IdStatusPedido  int32   `json:"IdStatusPedido"`
	CodigoPedidoERP string  `json:"CodigoPedidoERP"`
	ValorFaturado   float32 `json:"ValorFaturado"`
	Observacao      string  `json:"Observacao"`
	Status          string  `json:"Status"`
	layout          string  `json:"layout"`
}
