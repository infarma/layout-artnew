package arquivoDePedidoTest

import (
	"fmt"
	"layout-artnew/arquivoDePedido"
	"os"
	"testing"
)

func TestArquivoPedido(t *testing.T) {

	file, err := os.Open("../testFile/0011900053.ped")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo,err2 := arquivoDePedido.GetStruct(file)

	var cabecalho arquivoDePedido.CabecalhoPedido
	cabecalho.TipoRegistro = 1
	cabecalho.CodigoRepresentante = 55555
	cabecalho.CodigoCliente = 7777777
	cabecalho.DataEmissao = "9999999999"
	cabecalho.HoraEmissao = "55555"
	cabecalho.CodigoPedidoWeb = "9999999999"
	cabecalho.Observacao  = "8888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888"
	cabecalho.CodigoPrazo  = 333
	cabecalho.ChavePolitica = 4444

	if cabecalho.TipoRegistro != arquivo.CabecalhoPedido.TipoRegistro {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.TipoRegistro  )
		fmt.Println("cabecalho :", cabecalho.TipoRegistro  )
		t.Error("TipoRegistro não é compativel", err2)
	}
	if cabecalho.CodigoRepresentante != arquivo.CabecalhoPedido.CodigoRepresentante {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.CodigoRepresentante  )
		fmt.Println("cabecalho :", cabecalho.TipoRegistro  )
		t.Error("CodigoRepresentante não é compativel", err2)
	}
	if cabecalho.CodigoCliente != arquivo.CabecalhoPedido.CodigoCliente {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.CodigoCliente  )
		fmt.Println("cabecalho :", cabecalho.CodigoCliente  )
		t.Error("CodigoCliente não é compativel", err2)
	}
	if cabecalho.DataEmissao != arquivo.CabecalhoPedido.DataEmissao {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.DataEmissao  )
		fmt.Println("cabecalho :", cabecalho.DataEmissao  )
		t.Error("DataEmissao não é compativel", err2)
	}
	if cabecalho.HoraEmissao != arquivo.CabecalhoPedido.HoraEmissao {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.HoraEmissao  )
		fmt.Println("cabecalho :", cabecalho.HoraEmissao  )
		t.Error("HoraEmissao não é compativel", err2)
	}
	if cabecalho.CodigoPedidoWeb != arquivo.CabecalhoPedido.CodigoPedidoWeb {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.CodigoPedidoWeb  )
		fmt.Println("cabecalho :", cabecalho.TipoRegistro  )
		t.Error("CodigoPedidoWeb não é compativel", err2)
	}
	if cabecalho.Observacao != arquivo.CabecalhoPedido.Observacao {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.Observacao  )
		fmt.Println("cabecalho :", cabecalho.TipoRegistro  )
		t.Error("Observacao não é compativel", err2)
	}
	if cabecalho.CodigoPrazo != arquivo.CabecalhoPedido.CodigoPrazo {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.CodigoPrazo  )
		fmt.Println("cabecalho :", cabecalho.CodigoPrazo  )
		t.Error("CodigoPrazo não é compativel", err2)
	}
	if cabecalho.ChavePolitica != arquivo.CabecalhoPedido.ChavePolitica {
		fmt.Println("arquivo:", arquivo.CabecalhoPedido.ChavePolitica  )
		fmt.Println("cabecalho :", cabecalho.ChavePolitica  )
		t.Error("ChavePolitica não é compativel", err2)
	}

	// itens pedido

	var itens arquivoDePedido.ItensPedido
	itens.TipoRegistro = int32(2)
	itens.CodigoItem = int32(666666)
	itens.Quantidade = int32(555555)
	itens.Desconto = float32(44.22)
	itens.PrecoUnitario  = float32(55555.4444)
	itens.FaotrConversao= int32(55555)
	itens.ChavePolitica = int32(4444)
	itens.PrecoUnitarioApresentacao = float32(55555.4444)
	itens.TipoSaida = "1"


	if itens.TipoRegistro != arquivo.ItensPedido[0].TipoRegistro {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].TipoRegistro  )
		fmt.Println("itens :", itens.TipoRegistro  )
		t.Error("ChavePolitica não é compativel", err2)
	}
	if itens.CodigoItem != arquivo.ItensPedido[0].CodigoItem {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].CodigoItem  )
		fmt.Println("itens :", itens.CodigoItem  )
		t.Error("CodigoItem não é compativel", err2)
	}
	if itens.Quantidade != arquivo.ItensPedido[0].Quantidade {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].Quantidade  )
		fmt.Println("itens :", itens.Quantidade  )
		t.Error("Quantidade não é compativel", err2)
	}
	if itens.Desconto != arquivo.ItensPedido[0].Desconto {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].Desconto  )
		fmt.Println("itens :", itens.Desconto  )
		t.Error("Desconto não é compativel", err2)
	}
	if itens.PrecoUnitario != arquivo.ItensPedido[0].PrecoUnitario {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].PrecoUnitario  )
		fmt.Println("itens :", itens.PrecoUnitario  )
		t.Error("PrecoUnitario não é compativel", err2)
	}
	if itens.FaotrConversao != arquivo.ItensPedido[0].FaotrConversao {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].FaotrConversao  )
		fmt.Println("itens :", itens.FaotrConversao  )
		t.Error("FaotrConversao não é compativel", err2)
	}
	if itens.ChavePolitica != arquivo.ItensPedido[0].ChavePolitica {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].ChavePolitica  )
		fmt.Println("itens :", itens.ChavePolitica  )
		t.Error("ChavePolitica não é compativel", err2)
	}
	if itens.PrecoUnitarioApresentacao != arquivo.ItensPedido[0].PrecoUnitarioApresentacao {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].PrecoUnitarioApresentacao  )
		fmt.Println("itens :", itens.PrecoUnitarioApresentacao  )
		t.Error("PrecoUnitarioApresentacao não é compativel", err2)
	}
	if itens.TipoSaida != arquivo.ItensPedido[0].TipoSaida	 {
		fmt.Println("arquivo:", arquivo.ItensPedido[0].TipoSaida  )
		fmt.Println("itens :", itens.TipoSaida  )
		t.Error("TipoSaida não é compativel", err2)
	}

}
